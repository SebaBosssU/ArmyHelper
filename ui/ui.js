define('two/administrator/ui', [
    'two/administrator',
    'two/locale',
    'two/ui',
    'two/FrontButton',
    'two/utils'
], function(
    ArmyHelper,
    Locale,
    Interface,
    FrontButton,
    utils
) {
    var ui
    var opener
    var $window
    var $assignAll
    var $assignName
    var $assignGroup
    var $assignNG
    var $checkArmy
    var $SpearA
    var $SpearO
    var $SpearI
    var $SpearS
    var $SpearR
    var $SpearT
    var $SwordA
    var $SwordO
    var $SwordI
    var $SwordS
    var $SwordR
    var $SwordT
    var $AxeA
    var $AxeO
    var $AxeI
    var $AxeS
    var $AxeR
    var $AxeT
    var $ArcherA
    var $ArcherO
    var $ArcherI
    var $ArcherS
    var $ArcherR
    var $ArcherT
    var $LightCavalryA
    var $LightCavalryO
    var $LightCavalryI
    var $LightCavalryS
    var $LightCavalryR
    var $LightCavalryT
    var $MountedArcherA
    var $MountedArcherO
    var $MountedArcherI
    var $MountedArcherS
    var $MountedArcherR
    var $MountedArcherT
    var $HeavyCavalryA
    var $HeavyCavalryO
    var $HeavyCavalryI
    var $HeavyCavalryS
    var $HeavyCavalryR
    var $HeavyCavalryT
    var $RamA
    var $RamO
    var $RamI
    var $RamS
    var $RamR
    var $RamT
    var $CatapultA
    var $CatapultO
    var $CatapultI
    var $CatapultS
    var $CatapultR
    var $CatapultT
    var $TrebuchetA
    var $TrebuchetO
    var $TrebuchetI
    var $TrebuchetS
    var $TrebuchetR
    var $TrebuchetT
    var $BerserkerA
    var $BerserkerO
    var $BerserkerI
    var $BerserkerS
    var $BerserkerR
    var $BerserkerT
    var $SnobA
    var $SnobO
    var $SnobI
    var $SnobS
    var $SnobR
    var $SnobT
    var $KnightA
    var $KnightO
    var $KnightI
    var $KnightS
    var $KnightR
    var $KnightT
    var groups
    var $group1
    var $group2
    var $selectedGroup1
    var $selectedGroup2
    /**
     * Bind all elements events and notifications.
     */
    var bindEvents = function() {
        $assignAll.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var presets = []
            var interval = 5000
            var toAssign = {}
            var done = {
                content: {}
            }

            function getPresets() {
                socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                    for (i = 0; i < data.presets.length; i++) {
                        presets.push(data.presets[i].id)
                    }
                })
                setTimeout(toObject, 3000)
            }

            function toObject() {
                var toAssign = presets.reduce(function(acc, cur, i) {
                    acc[i] = cur
                    return acc
                }, {})
                done.content = toAssign
                setTimeout(assignPresets, 3000)
            }

            function assignPresets() {
                villages.forEach(function(village, index) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.ASSIGN_PRESETS, {
                            village_id: village.getId(),
                            preset_ids: done.content
                        })
                    }, index * interval * Math.random())
                })
                utils.emitNotif('success', Locale('administrator', 'assigned'))
            }
            getPresets()
        })
        $assignName.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var presets = []
            var interval = 5000
            var toAssign = {}
            var done = {
                content: {}
            }
            var name = document.getElementById('presetsName').value

            function getPresets() {
                socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                    for (i = 0; i < data.presets.length; i++) {
                        if ((data.presets[i].name).indexOf(name) >= 0) {
                            presets.push(data.presets[i].id)
                        }
                    }
                })
                setTimeout(toObject, 3000)
            }

            function toObject() {
                var toAssign = presets.reduce(function(acc, cur, i) {
                    acc[i] = cur
                    return acc
                }, {})
                done.content = toAssign
                setTimeout(assignPresets, 3000)
            }

            function assignPresets() {
                villages.forEach(function(village, index) {
                    nameN = village.getName()
                    setTimeout(function() {
                        socketService.emit(routeProvider.ASSIGN_PRESETS, {
                            village_id: village.getId(),
                            preset_ids: done.content
                        })
                        var name = village.getName()
                    }, index * interval * Math.random())
                })
                utils.emitNotif('success', Locale('administrator', 'assignedN'))
            }
            getPresets()
        })
        $assignGroup.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var groupList = modelDataService.getGroupList()
            var selectedGroup1 = $selectedGroup1[0].dataset.value
            var groupVillages = groupList.getGroupVillageIds(selectedGroup1)
            var presets = []
            var interval = 5000
            var toAssign = {}
            var done = {
                content: {}
            }

            function getPresets() {
                socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                    for (i = 0; i < data.presets.length; i++) {
                        presets.push(data.presets[i].id)
                    }
                })
                setTimeout(toObject, 3000)
            }

            function toObject() {
                var toAssign = presets.reduce(function(acc, cur, i) {
                    acc[i] = cur
                    return acc
                }, {})
                done.content = toAssign
                setTimeout(assignPresets, 3000)
            }

            function assignPresets() {
                groupVillages.forEach(function(village, index) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.ASSIGN_PRESETS, {
                            village_id: village,
                            preset_ids: done.content
                        })
                    }, index * interval * Math.random())
                })
                utils.emitNotif('success', Locale('administrator', 'assignedG'))
            }
            getPresets()
        })
        $assignNG.on('click', function() {			
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var groupList = modelDataService.getGroupList()
            var selectedGroup2 = $selectedGroup2[0].dataset.value
            var groupVillages = groupList.getGroupVillageIds(selectedGroup2)
            var presets = []
            var interval = 5000
            var toAssign = {}
            var done = {
                content: {}
            }
            var name = document.getElementById('presetsN').value

            function getPresets() {
                socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                    for (i = 0; i < data.presets.length; i++) {
                        if ((data.presets[i].name).indexOf(name) >= 0) {
                            presets.push(data.presets[i].id)
                        }
                    }
                })
                setTimeout(toObject, 3000)
            }

            function toObject() {
                var toAssign = presets.reduce(function(acc, cur, i) {
                    acc[i] = cur
                    return acc
                }, {})
                done.content = toAssign
                setTimeout(assignPresets, 3000)
            }

            function assignPresets() {
                groupVillages.forEach(function(village, index) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.ASSIGN_PRESETS, {
                            village_id: village,
                            preset_ids: done.content
                        })
                    }, index * interval * Math.random())
                })
                utils.emitNotif('success', Locale('administrator', 'assignedG'))
            }
            getPresets()
        })
        $checkArmy.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            SpearA = 0
            SpearO = 0
            SpearI = 0
            SpearS = 0
            SpearR = 0
            SpearT = 0
            SwordA = 0
            SwordO = 0
            SwordI = 0
            SwordS = 0
            SwordR = 0
            SwordT = 0
            AxeA = 0
            AxeO = 0
            AxeI = 0
            AxeS = 0
            AxeR = 0
            AxeT = 0
            ArcherA = 0
            ArcherO = 0
            ArcherI = 0
            ArcherS = 0
            ArcherR = 0
            ArcherT = 0
            LightCavalryA = 0
            LightCavalryO = 0
            LightCavalryI = 0
            LightCavalryS = 0
            LightCavalryR = 0
            LightCavalryT = 0
            MountedArcherA = 0
            MountedArcherO = 0
            MountedArcherI = 0
            MountedArcherS = 0
            MountedArcherR = 0
            MountedArcherT = 0
            HeavyCavalryA = 0
            HeavyCavalryO = 0
            HeavyCavalryI = 0
            HeavyCavalryS = 0
            HeavyCavalryR = 0
            HeavyCavalryT = 0
            RamA = 0
            RamO = 0
            RamI = 0
            RamS = 0
            RamR = 0
            RamT = 0
            CatapultA = 0
            CatapultO = 0
            CatapultI = 0
            CatapultS = 0
            CatapultR = 0
            CatapultT = 0
            TrebuchetA = 0
            TrebuchetO = 0
            TrebuchetI = 0
            TrebuchetS = 0
            TrebuchetR = 0
            TrebuchetT = 0
            BerserkerA = 0
            BerserkerO = 0
            BerserkerI = 0
            BerserkerS = 0
            BerserkerR = 0
            BerserkerT = 0
            SnobA = 0
            SnobO = 0
            SnobI = 0
            SnobS = 0
            SnobR = 0
            SnobT = 0
            KnightA = 0
            KnightO = 0
            KnightI = 0
            KnightS = 0
            KnightR = 0
            KnightT = 0

            function checkArmy() {
                villages.forEach(function(village) {
                    var unitInfo = village.unitInfo
                    var units = unitInfo.units
                    var spear = units.spear
                    var sword = units.sword
                    var axe = units.axe
                    var archer = units.archer
                    var light_cavalry = units.light_cavalry
                    var mounted_archer = units.mounted_archer
                    var heavy_cavalry = units.heavy_cavalry
                    var ram = units.ram
                    var catapult = units.catapult
                    var trebuchet = units.trebuchet
                    var doppelsoldner = units.doppelsoldner
                    var snob = units.snob
                    var knight = units.knight
                    SpearA += spear.available
                    SpearO += spear.own
                    SpearI += spear.in_town
                    SpearS += spear.support
                    SpearR += spear.recruiting
                    SpearT += spear.total
                    SwordA += sword.available
                    SwordO += sword.own
                    SwordI += sword.in_town
                    SwordS += sword.support
                    SwordR += sword.recruiting
                    SwordT += sword.total
                    AxeA += axe.available
                    AxeO += axe.own
                    AxeI += axe.in_town
                    AxeS += axe.support
                    AxeR += axe.recruiting
                    AxeT += axe.total
                    ArcherA += archer.available
                    ArcherO += archer.own
                    ArcherI += archer.in_town
                    ArcherS += archer.support
                    ArcherR += archer.recruiting
                    ArcherT += archer.total
                    LightCavalryA += light_cavalry.available
                    LightCavalryO += light_cavalry.own
                    LightCavalryI += light_cavalry.in_town
                    LightCavalryS += light_cavalry.support
                    LightCavalryR += light_cavalry.recruiting
                    LightCavalryT += light_cavalry.total
                    MountedArcherA += mounted_archer.available
                    MountedArcherO += mounted_archer.own
                    MountedArcherI += mounted_archer.in_town
                    MountedArcherS += mounted_archer.support
                    MountedArcherR += mounted_archer.recruiting
                    MountedArcherT += mounted_archer.total
                    HeavyCavalryA += heavy_cavalry.available
                    HeavyCavalryO += heavy_cavalry.own
                    HeavyCavalryI += heavy_cavalry.in_town
                    HeavyCavalryS += heavy_cavalry.support
                    HeavyCavalryR += heavy_cavalry.recruiting
                    HeavyCavalryT += heavy_cavalry.total
                    RamA += ram.available
                    RamO += ram.own
                    RamI += ram.in_town
                    RamS += ram.support
                    RamR += ram.recruiting
                    RamT += ram.total
                    CatapultA += catapult.available
                    CatapultO += catapult.own
                    CatapultI += catapult.in_town
                    CatapultS += catapult.support
                    CatapultR += catapult.recruiting
                    CatapultT += catapult.total
                    TrebuchetA += trebuchet.available
                    TrebuchetO += trebuchet.own
                    TrebuchetI += trebuchet.in_town
                    TrebuchetS += trebuchet.support
                    TrebuchetR += trebuchet.recruiting
                    TrebuchetT += trebuchet.total
                    BerserkerA += doppelsoldner.available
                    BerserkerO += doppelsoldner.own
                    BerserkerI += doppelsoldner.in_town
                    BerserkerS += doppelsoldner.support
                    BerserkerR += doppelsoldner.recruiting
                    BerserkerT += doppelsoldner.total
                    SnobA += snob.available
                    SnobO += snob.own
                    SnobI += snob.in_town
                    SnobS += snob.support
                    SnobR += snob.recruiting
                    SnobT += snob.total
                    KnightA += knight.available
                    KnightO += knight.own
                    KnightI += knight.in_town
                    KnightS += knight.support
                    KnightR += knight.recruiting
                    KnightT += knight.total
                })
                setTimeout(showArmy, 3000)
            }

            function showArmy() {
                $SpearA.html(SpearA)
                $SpearO.html(SpearO)
                $SpearI.html(SpearI)
                $SpearS.html(SpearS)
                $SpearR.html(SpearR)
                $SpearT.html(SpearT)
                $SwordA.html(SwordA)
                $SwordO.html(SwordO)
                $SwordI.html(SwordI)
                $SwordS.html(SwordS)
                $SwordR.html(SwordR)
                $SwordT.html(SwordT)
                $AxeA.html(AxeA)
                $AxeO.html(AxeO)
                $AxeI.html(AxeI)
                $AxeS.html(AxeS)
                $AxeR.html(AxeR)
                $AxeT.html(AxeT)
                $ArcherA.html(ArcherA)
                $ArcherO.html(ArcherO)
                $ArcherI.html(ArcherI)
                $ArcherS.html(ArcherS)
                $ArcherR.html(ArcherR)
                $ArcherT.html(ArcherT)
                $LightCavalryA.html(LightCavalryA)
                $LightCavalryO.html(LightCavalryO)
                $LightCavalryI.html(LightCavalryI)
                $LightCavalryS.html(LightCavalryS)
                $LightCavalryR.html(LightCavalryR)
                $LightCavalryT.html(LightCavalryT)
                $MountedArcherA.html(MountedArcherA)
                $MountedArcherO.html(MountedArcherO)
                $MountedArcherI.html(MountedArcherI)
                $MountedArcherS.html(MountedArcherS)
                $MountedArcherR.html(MountedArcherR)
                $MountedArcherT.html(MountedArcherT)
                $HeavyCavalryA.html(HeavyCavalryA)
                $HeavyCavalryO.html(HeavyCavalryO)
                $HeavyCavalryI.html(HeavyCavalryI)
                $HeavyCavalryS.html(HeavyCavalryS)
                $HeavyCavalryR.html(HeavyCavalryR)
                $HeavyCavalryT.html(HeavyCavalryT)
                $RamA.html(RamA)
                $RamO.html(RamO)
                $RamI.html(RamI)
                $RamS.html(RamS)
                $RamR.html(RamR)
                $RamT.html(RamT)
                $CatapultA.html(CatapultA)
                $CatapultO.html(CatapultO)
                $CatapultI.html(CatapultI)
                $CatapultS.html(CatapultS)
                $CatapultR.html(CatapultR)
                $CatapultT.html(CatapultT)
                $TrebuchetA.html(TrebuchetA)
                $TrebuchetO.html(TrebuchetO)
                $TrebuchetI.html(TrebuchetI)
                $TrebuchetS.html(TrebuchetS)
                $TrebuchetR.html(TrebuchetR)
                $TrebuchetT.html(TrebuchetT)
                $BerserkerA.html(BerserkerA)
                $BerserkerO.html(BerserkerO)
                $BerserkerI.html(BerserkerI)
                $BerserkerS.html(BerserkerS)
                $BerserkerR.html(BerserkerR)
                $BerserkerT.html(BerserkerT)
                $SnobA.html(SnobA)
                $SnobO.html(SnobO)
                $SnobI.html(SnobI)
                $SnobS.html(SnobS)
                $SnobR.html(SnobR)
                $SnobT.html(SnobT)
                $KnightA.html(KnightA)
                $KnightO.html(KnightO)
                $KnightI.html(KnightI)
                $KnightS.html(KnightS)
                $KnightR.html(KnightR)
                $KnightT.html(KnightT)
                utils.emitNotif('success', Locale('administrator', 'calculated'))
            }
            checkArmy()
        })
        rootScope.$on(eventTypeProvider.GROUPS_UPDATED, function () {
            updateGroupVillages()
        })
        $selectedGroup1.on('selectSelected', function() {
	        selectedGroup1 = $selectedGroup1[0].dataset.value
        })
        $selectedGroup2.on('selectSelected', function() {
	        selectedGroup2 = $selectedGroup2[0].dataset.value
        })
    }
    var AdministratorInterface = function() {
        groups = modelDataService.getGroupList().getGroups()
        ui = new Interface('ArmyHelper', {
            activeTab: 'presets',
            template: '__administrator_html_window',
            css: '__administrator_css_style',
            replaces: {
                locale: Locale,
                version: ArmyHelper.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('administrator', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        $assignAll = $window.find('.item-asignAll span')
        $assignName = $window.find('.item-asignName span')
        $assignGroup = $window.find('.item-asignGroup span')
        $assignNG = $window.find('.item-asignNG span')
        $checkArmy = $window.find('.item-check span')
        $SpearA = $window.find('.item-spear-a')
        $SpearO = $window.find('.item-spear-o')
        $SpearI = $window.find('.item-spear-i')
        $SpearS = $window.find('.item-spear-s')
        $SpearR = $window.find('.item-spear-r')
        $SpearT = $window.find('.item-spear-t')
        $SwordA = $window.find('.item-sword-a')
        $SwordO = $window.find('.item-sword-o')
        $SwordI = $window.find('.item-sword-i')
        $SwordS = $window.find('.item-sword-s')
        $SwordR = $window.find('.item-sword-r')
        $SwordT = $window.find('.item-sword-t')
        $AxeA = $window.find('.item-axe-a')
        $AxeO = $window.find('.item-axe-o')
        $AxeI = $window.find('.item-axe-i')
        $AxeS = $window.find('.item-axe-s')
        $AxeR = $window.find('.item-axe-r')
        $AxeT = $window.find('.item-axe-t')
        $ArcherA = $window.find('.item-archer-a')
        $ArcherO = $window.find('.item-archer-o')
        $ArcherI = $window.find('.item-archer-i')
        $ArcherS = $window.find('.item-archer-s')
        $ArcherR = $window.find('.item-archer-r')
        $ArcherT = $window.find('.item-archer-t')
        $LightCavalryA = $window.find('.item-lc-a')
        $LightCavalryO = $window.find('.item-lc-o')
        $LightCavalryI = $window.find('.item-lc-i')
        $LightCavalryS = $window.find('.item-lc-s')
        $LightCavalryR = $window.find('.item-lc-r')
        $LightCavalryT = $window.find('.item-lc-t')
        $MountedArcherA = $window.find('.item-ma-a')
        $MountedArcherO = $window.find('.item-ma-o')
        $MountedArcherI = $window.find('.item-ma-i')
        $MountedArcherS = $window.find('.item-ma-s')
        $MountedArcherR = $window.find('.item-ma-r')
        $MountedArcherT = $window.find('.item-ma-t')
        $HeavyCavalryA = $window.find('.item-hc-a')
        $HeavyCavalryO = $window.find('.item-hc-o')
        $HeavyCavalryI = $window.find('.item-hc-i')
        $HeavyCavalryS = $window.find('.item-hc-s')
        $HeavyCavalryR = $window.find('.item-hc-r')
        $HeavyCavalryT = $window.find('.item-hc-t')
        $RamA = $window.find('.item-ram-a')
        $RamO = $window.find('.item-ram-o')
        $RamI = $window.find('.item-ram-i')
        $RamS = $window.find('.item-ram-s')
        $RamR = $window.find('.item-ram-r')
        $RamT = $window.find('.item-ram-t')
        $CatapultA = $window.find('.item-catapult-a')
        $CatapultO = $window.find('.item-catapult-o')
        $CatapultI = $window.find('.item-catapult-i')
        $CatapultS = $window.find('.item-catapult-s')
        $CatapultR = $window.find('.item-catapult-r')
        $CatapultT = $window.find('.item-catapult-t')
        $TrebuchetA = $window.find('.item-trebuchet-a')
        $TrebuchetO = $window.find('.item-trebuchet-o')
        $TrebuchetI = $window.find('.item-trebuchet-i')
        $TrebuchetS = $window.find('.item-trebuchet-s')
        $TrebuchetR = $window.find('.item-trebuchet-r')
        $TrebuchetT = $window.find('.item-trebuchet-t')
        $BerserkerA = $window.find('.item-berserker-a')
        $BerserkerO = $window.find('.item-berserker-o')
        $BerserkerI = $window.find('.item-berserker-i')
        $BerserkerS = $window.find('.item-berserker-s')
        $BerserkerR = $window.find('.item-berserker-r')
        $BerserkerT = $window.find('.item-berserker-t')
        $SnobA = $window.find('.item-snob-a')
        $SnobO = $window.find('.item-snob-o')
        $SnobI = $window.find('.item-snob-i')
        $SnobS = $window.find('.item-snob-s')
        $SnobR = $window.find('.item-snob-r')
        $SnobT = $window.find('.item-snob-t')
        $KnightA = $window.find('.item-knight-a')
        $KnightO = $window.find('.item-knight-o')
        $KnightI = $window.find('.item-knight-i')
        $KnightS = $window.find('.item-knight-s')
        $KnightR = $window.find('.item-knight-r')
        $KnightT = $window.find('.item-knight-t')
        $group1 = $window.find('.group1')
        $group2 = $window.find('.group2')
        $selectedGroup1 = $window.find('.group1')
        $selectedGroup2 = $window.find('.group2')
        updateGroupVillages()
        updateGroupVillagesN()
        bindEvents()
        ArmyHelper.interfaceInitialized = true
        return ui
    }
    var appendSelectData = function appendSelectData ($data, data) {
        var dataElem = document.createElement('span')

        for (var key in data) {
            dataElem.dataset[key] = data[key]
        }

        $data.append(dataElem)
    }
    var updateGroupVillages = function updateGroupVillages () {
        var $selectedOption = $group1.find('.custom-select-handler').html('')
        var $data = $group1.find('.custom-select-data').html('')

        for (var id in groups) {
            var name = groups[id].name

            appendSelectData($data, {
                name: name,
                value: id,
                icon: groups[id].icon
            })

            $group1.append($data)
        }
    }
    var updateGroupVillagesN = function updateGroupVillagesN () {
        var $selectedOption2 = $group2.find('.custom-select-handler').html('')
        var $data2 = $group2.find('.custom-select-data').html('')

        for (var id in groups) {
            var name = groups[id].name

            appendSelectData($data2, {
                name: name,
                value: id,
                icon: groups[id].icon
            })

            $group2.append($data2)
        }
    }
    ArmyHelper.interface = function() {
        ArmyHelper.interface = AdministratorInterface()
    }
})