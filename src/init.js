require([
    'two/ready',
    'two/administrator',
    'two/administrator/ui'
], function (
    ready,
    ArmyHelper
) {
    if (ArmyHelper.initialized) {
        return false
    }

    ready(function () {
        ArmyHelper.init()
        ArmyHelper.interface()
        ArmyHelper.run()
    })
})
