define('two/administrator', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var ArmyHelper = {}
    ArmyHelper.version = '__administrator_version'
    ArmyHelper.init = function () {
        Locale.create('administrator', __administrator_locale, 'pl')

        ArmyHelper.initialized = true
    }
    ArmyHelper.run = function () {
        if (!ArmyHelper.interfaceInitialized) {
            throw new Error('ArmyHelper interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return ArmyHelper
})